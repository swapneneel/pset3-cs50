// Helper functions for music
// This is the task after bday.txt
#include <cs50.h>
#include <stdio.h>
#include <stdlib.h>  //for atoi
//int atoi(string str);
#include <math.h>
#include <string.h>
#include <ctype.h>

#include "helpers.h"

// Converts a fraction formatted as X/Y to eighths
int duration(string fraction)
{
    // TODO
    int d, num, dem;
    for (int i = 0; i < 3; i++)
    {
        if (isdigit(fraction[i]))
        {
            if (i == 0)
            {
                num = atoi(&fraction[i]);
            }
            if (i == 2)
            {
                dem = atoi(&fraction[i]);
            }
        }
    }
    d = (num * 8) / dem;
    return d;
}

// Calculates frequency (in Hz) of a note
int frequency(string note)
{
    // TODO
    //string sharp = "A A# B C C# D D# E F F# G G#", flap = "A Bb B C Db D Eb E F Gb G Ab";
    int n = strlen(note), oct = 0;

    double f, f_note;  //, freq
    //The f_notes are writen in accordance with octave 4
    if (n == 3)  //with the accidentals
    {
        if (note[1] == '#')  //sharp
        {
            if (note[0] == 'A')
            {
                f_note = 440 * pow(2, 0.0833333333);
            }
            else if (note[0] == 'C')
            {
                f_note = 440 / pow(pow(2, 0.0833333333), 8);  //One semitone down is division by pow(2, 1/12)
            }
            else if (note[0] == 'D')
            {
                f_note = 440 / pow(pow(2, 0.0833333333), 6);
            }
            else if (note[0] == 'F')
            {
                f_note = 440 / pow(pow(2, 0.0833333333), 3);
            }
            else  //for G#
            {
                f_note = 440 / pow(2, 0.0833333333);
            }
        }
        if (note[1] == 'b')  //For flat
        {
            if (note[0] == 'A')
            {
                f_note = 440 / pow(2, 0.0833333333);  //One seminote low so divi
            }
            else if (note[0] == 'B')
            {
                f_note = 440 * pow(2, 0.0833333333);
            }
            else if (note[0] == 'D')
            {
                f_note = 440 / pow(pow(2, 0.0833333333), 8);
            }
            else if (note[0] == 'E')
            {
                f_note = 440 / pow(pow(2, 0.0833333333), 6);
            }
            else  //for Gb
            {
                f_note = 440 / pow(pow(2, 0.0833333333), 3);
            }
        }

        oct = atoi(&note[2]);  //It is the third element of the note string, the octave number
    }
    else  //with no accidentals i.e. n == 2 //string sharp = "A A# B C C# D D# E F F# G G#"
    {
        if (note[0] == 'A')  //This is the base of the frequencies
        {
            f_note = 440;
        }
        else if (note[0] == 'B')
        {
            f_note = 440 * pow(pow(2, 0.0833333333), 2);  //B4 is 2 seminote above A4
        }
        else if (note[0] == 'C')  //C4
        {
            f_note = 440 / pow(pow(2, 0.0833333333), 9);  //pow(2, 9/12);
        }
        else if (note[0] == 'D')
        {
            f_note = 440 / pow(pow(2, 0.0833333333), 7);
        }
        else if (note[0] == 'E')
        {
            f_note = 440 / pow(pow(2, 0.0833333333), 5);
        }
        else if (note[0] == 'F')
        {
            f_note = 440 / pow(pow(2, 0.0833333333), 4);
        }
        else  //for G4
        {
            f_note = 440 / pow(pow(2, 0.0833333333), 2);
        }

        oct = atoi(&note[1]);  //This the char of the octave number converted to int
    }

    if (oct < 4)
    {
        f = f_note / pow(2, (4 - oct));
    }
    else if (oct > 4)
    {
        f = f_note * pow(2, (oct - 4));
    }
    else  //oct is 4
    {
        f = f_note;
    }

    double freq = round(f);  //round is of type input and output double
    int frequency = freq;
    return frequency;
}

// Determines whether a string represents a rest
bool is_rest(string s)
{
    // TODO  A rest would look like @1/8
    if (strncmp(s, "", 1))
    {
        return false;
    }
    else
    {
        return true;
    }
}
